from django.shortcuts import render


from  rest_framework.response import  Response
from rest_framework import generics,mixins
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated,IsAdminUser
from rest_framework_csv.renderers import CSVRenderer

from todo.serializers import  *
from todo.models import Todo
from rest_framework.settings import api_settings
from rest_framework_csv import renderers as r

from .permissions import IsOwnerOrReadOnly

class TodoList(mixins.ListModelMixin,mixins.CreateModelMixin, generics.GenericAPIView):
    serializer_class = TodoSerializer
    permission_classes = (IsAuthenticated,IsOwnerOrReadOnly)
    filterset_fields = ['title']
    search_fields = ['title','description',]


    #
    def get_queryset(self):

        user=self.request.user
        if user.is_superuser or user.is_staff:
             return  Todo.objects.all()
        if user.is_active:
            return  Todo.objects.filter(user=user)

        else:
            return Todo.objects.none()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    def get(self, request, *args, **kwargs):
            return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
            return self.create(request, *args, **kwargs)




class TodoDetail(generics.GenericAPIView,
                       mixins.UpdateModelMixin,
                       mixins.RetrieveModelMixin,
                       mixins.DestroyModelMixin):
    serializer_class = TodoDetailSerializer
    queryset = Todo.objects.all()


    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly)



    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
            return self.partial_update(request, *args, **kwargs)


    def delete(self, request, *args, **kwargs):

            return self.destroy(request, *args, **kwargs)


