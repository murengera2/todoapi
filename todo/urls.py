from  todo.views import  TodoList,TodoDetail
from  django.urls import  path

urlpatterns=[
    path("todo-item/",TodoList.as_view()),
    path('todo-item/<slug:pk>', TodoDetail.as_view(), name='todo-ITEM'),

]
