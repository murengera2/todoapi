from django.db import models
from  account.managers import CustomUserManager
from  django.contrib.auth.models import  AbstractBaseUser,PermissionsMixin
from django.utils.translation import  gettext_lazy as  _
import random,string
class User(AbstractBaseUser,PermissionsMixin):
    name=models.CharField(_('name'),max_length=100)
    email=models.EmailField(_('email address'),unique=True)
    username=models.CharField(_('username'),unique=True,max_length=100)
    is_staff=models.BooleanField(default=False)
    is_active =models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    USERNAME_FIELD='username'
    REQUIRED_FIELDS = ['name','email']

    objects=CustomUserManager()




def generate_password():
        """ Generates a digit based default password """
        list = [random.choice(range(0, 9)) for i in range(0, 10)]
        code = ''.join(str(i) for i in list)
        return code

def generate_code():
        key = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
        return key

class Verification(models.Model):
        code = models.CharField(max_length=8, blank=True)
        user = models.ForeignKey(User, on_delete=models.CASCADE)
        categories = {
            ('activation', 'activation'),
            ('reset', 'reset')
        }
        category = models.CharField(max_length=100, choices=categories, default='activation')
        is_used = models.BooleanField(default=False)

        def __str__(self):
            return self.code

        def save(self, *args, **kwargs):
            self.code = generate_code()
            super(Verification, self).save(*args, **kwargs)
